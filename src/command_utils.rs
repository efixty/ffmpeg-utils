use async_process::Command;

pub fn ffmpeg_command_base(source: &str) -> Command {
    let mut command = Command::new("ffmpeg");
    command.arg("-i").arg(source);
    return command;
}