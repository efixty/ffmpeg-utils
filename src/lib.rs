pub mod video;
pub mod audio;
mod command_utils;

#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;

    #[test]
    fn extract_audio_from_stream() {
        let url = "https://rr2---sn-0jvh-x8o6.googlevideo.com/videoplayback?expire=1674167235&ei=Y2_JY63uHomoWdLrqKgP&ip=37.252.95.92&id=o-AMxOejUZXUE7RjZh3eX9_V7XSLJQH8XXwXt0bJfyz-Hj&itag=249&source=youtube&requiressl=yes&mh=EA&mm=31%2C29&mn=sn-0jvh-x8o6%2Csn-nv47lnsk&ms=au%2Crdu&mv=m&mvi=2&pl=22&initcwndbps=522500&vprv=1&mime=audio%2Fwebm&ns=4k4Iuy76yfTFJu-29RrL44cK&gir=yes&clen=2289970&dur=336.181&lmt=1577072986502889&mt=1674145307&fvip=3&keepalive=yes&fexp=24007246&c=WEB&txp=5431432&n=ZGKstQzj_8djDO5&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cgir%2Cclen%2Cdur%2Clmt&sig=AOq0QJ8wRgIhAKe37OV3-IGnvpO8hLmRlD-4sCVms6EL1ldkNs9wCtnVAiEA5xdi9ApyLkqhyLGFuTLQlSxgXcG7tMEZjmAbNxLbcm8%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRAIgJExh_7hg5t7wiHc3W7ojUuWPOqJxBT5JIEPfFWmKIR8CID5og7cyWkOXhbSAV6q6SnbRFkw4_w95AuESB01g02sU";
        let final_name = "out.mp3";
        fs::remove_file(final_name);
        let extraction_result = audio::extract(url, final_name, "HVOB - Dogs", 9);
        assert!(extraction_result.is_ok(),);
        assert!(extraction_result.map(|status| status.success()).unwrap(), "got error during audio extraction");
        assert!(fs::metadata(final_name).is_ok(), "file is not present");
    }
}
