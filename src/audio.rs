use crate::command_utils::ffmpeg_command_base;
use async_process::{Command, ExitStatus};

pub async fn extract(stream_url: &str, file_name: &str, track_name: &str, quality: i8) -> Result<ExitStatus, std::io::Error> {
    let mut audio_extraction: Command = ffmpeg_command_base(stream_url);
    let exit_status = audio_extraction
        .arg("-map")
        .arg("0:a")
        .arg("-acodec")
        .arg("libmp3lame")
        .arg("-q:a")
        .arg(quality.to_string())
        .arg("-metadata")
        .arg(format!("title={}", track_name))
        .arg(file_name)
        .spawn()
        .unwrap().status().await?;
    return Ok(exit_status);
}